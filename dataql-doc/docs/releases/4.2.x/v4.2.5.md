---
id: v4.2.5
title: v4.2.5 (2021-03-31)
---

# v4.2.5 (2021-03-31)

## 优化
- 修复潜在的 dataway NPE Bug
- hasor-dataql-fx 不在直接依赖 javassist、ognl、jsqlparser 转而都沉淀到 hasor-db 中。
- fxquery 能力也沉淀到 hasor-db 中。
- hasor-dataql-fx 中的 分页能力沉淀到 hasor-db。
